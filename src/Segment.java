public class Segment {
    private String clientId;
    private String segment;
    private int creditModifier;

    public Segment(String clientId, String segment, int creditModifier) {
        this.clientId = clientId;
        this.segment = segment;
        this.creditModifier = creditModifier;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public int getCreditModifier() {
        return creditModifier;
    }

    public void setCreditModifier(int creditModifier) {
        this.creditModifier = creditModifier;
    }

    @Override
    public String toString() {
        return "Segment{" +
                "clientId='" + clientId + '\'' +
                ", segment='" + segment + '\'' +
                ", creditModifier=" + creditModifier +
                '}';
    }
}
