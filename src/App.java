import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class App {
    static final double INTEREST = 10.0;

    public static List<Segment> segments = new ArrayList<>();

    public static int findCreditModifier(String clientId) { // kuidas ma võin "String clientId" asemel kohe sisestada siia client.id?
        for (int i = 0; i < segments.size(); i++) {
            Segment a = segments.get(i);
            if(a.getClientId().equals(clientId)) {
                return a.getCreditModifier();
            }
        }
        return 0;
    }

    public static double findCreditScore (int clientCreditModifier, int loanAmount, int loanPeriod){
        double creditScore = (double)clientCreditModifier / (double)loanAmount * loanPeriod;
        creditScore = Math.round(creditScore*100)/100.0d;
        return creditScore;
    }

    public static int responseToApplicationLoan (int clientCreditModifier, double creditScore, int loanAmount, int loanPeriod) {
        int maxLoanAmount;
        if(clientCreditModifier > 0){
            if(creditScore > 1){
                maxLoanAmount = (int)Math.round(clientCreditModifier / (1.0 / loanPeriod));
                if(maxLoanAmount >= 2000 && maxLoanAmount <= 10000){
                    return maxLoanAmount;
                } else {
                    return 10000;
                }
            } else {
                return loanAmount;
            }
        } else {
            return 0;
        }
    }

    public static int responseToApplicationPeriod (int clientCreditModifier, double creditScore, int loanAmount, int loanPeriod) {
        int minLoanPeriod;
        if(clientCreditModifier > 0){
            if(creditScore < 1){
                minLoanPeriod = (int)(1.0 / ((double)clientCreditModifier / (double)loanAmount));
                    if(minLoanPeriod >= 12 && minLoanPeriod <= 60){
                        return minLoanPeriod;
                    } else {
                        return 0;
                    }
            } else {
                return loanPeriod;
            }
        } else {
            return 0;
        }
    }

    public static double findMonthlyPayment(int loanPeriod, int loanAmount){
        int year = 12;
        double p = ((INTEREST/100)/year);
        double pPeriod = Math.pow((1 + p), loanPeriod);
        double monthlyPayment = Math.round(loanAmount * (p * pPeriod  / (pPeriod - 1)) * 100) / 100.0;
        return monthlyPayment;
    }


    public static void main(String[] args) throws IOException {
        final double interest = 10.0;

        List<String> creditModifierList = Files.readAllLines(Paths.get("segments.txt"));
        for(int i = 0; i < creditModifierList.size(); i++){
                String[] creditModifierLineParts = creditModifierList.get(i).split(" ");
                String clientId = creditModifierLineParts[0];
                String segment = creditModifierLineParts[1];
                int creditModifier = Integer.parseInt(creditModifierLineParts[2]);
                Segment s1 = new Segment(clientId, segment, creditModifier);
                segments.add(s1);
        }

        ApplicationForm client = new ApplicationForm("Jana", "Josepson", "37811150456", 2000, 12);
        System.out.println(client);

        int clientCreditModifier = findCreditModifier(client.getId());
        System.out.println("\n" + "Client credit modifier is: " + clientCreditModifier);

        double clientCreditScore = findCreditScore(clientCreditModifier, client.getLoanAmount(), client.getLoanPeriod());
        System.out.println("\n" + "Client credit score is: " + clientCreditScore);

        int responseLoan = responseToApplicationLoan(clientCreditModifier, clientCreditScore, client.getLoanAmount(), client.getLoanPeriod());
        int responsePeriod = responseToApplicationPeriod(clientCreditModifier, clientCreditScore, client.getLoanAmount(), client.getLoanPeriod());
        double responseMonthlyPayment = findMonthlyPayment(responsePeriod, responseLoan);

        System.out.println(
                "\n" + "Response to loan application" +
                        "\n" + "max loan amount: " + responseLoan + " eur" +
                        "\n" + "loan period: " + responsePeriod + " kuu(d)" +
                        "\n" + "interest rate: " + interest + " %" +
                        "\n" + "monthly payment: " + responseMonthlyPayment + " eur"
        );
    }
}
