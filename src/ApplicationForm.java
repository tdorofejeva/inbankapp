public class ApplicationForm {
    private String firstName;
    private String lastName;
    private String id;
    private int loanAmount;
    private int loanPeriod;

    public ApplicationForm(String firstName, String lastName, String id, int loanAmount, int loanPeriod) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
        this.loanAmount = loanAmount;
        this.loanPeriod = loanPeriod;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(int loanAmount) {
        this.loanAmount = loanAmount;
    }

    public int getLoanPeriod() {
        return loanPeriod;
    }

    public void setLoanPeriod(int loanPeriod) {
        this.loanPeriod = loanPeriod;
    }

    @Override
    public String toString() {
        String clientApplicationForm = "Application Form " + "\n" +
                "client name: " + firstName + " " + lastName + "\n" +
                "client ID: " + id + "\n" +
                "loan amount: " + loanAmount + "\n" +
                "loan period: " + loanPeriod;
        return clientApplicationForm;
    }
}
